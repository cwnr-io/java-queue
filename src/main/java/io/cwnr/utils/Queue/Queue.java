package io.cwnr.utils.Queue;

import java.util.List;

public interface Queue<E> {
    long size() throws Exception;
    boolean isEmpty() throws Exception;
    void clear() throws Exception;

    public E get() throws Exception;
    public List<E> get(long entryCount) throws Exception;
    public List<E> getAll() throws Exception;

    public E pool() throws Exception;
    public List<E> pool(long entryCount) throws Exception;
    public List<E> poolAll() throws Exception;

    public void add(E entry) throws Exception;
    public long addMany(List<E> entries) throws Exception;
}
