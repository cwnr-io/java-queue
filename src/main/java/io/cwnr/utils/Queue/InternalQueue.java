package io.cwnr.utils.Queue;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class InternalQueue<E> implements Queue<E> {
    private final ConcurrentLinkedQueue<E> queue;

    public InternalQueue() {
        this.queue = new ConcurrentLinkedQueue<E>();
    }

    @Override
    public long size() {
        return this.queue.size();
    }

    @Override
    public boolean isEmpty() {
        return this.queue.isEmpty();
    }

    @Override
    public void clear() {
        this.queue.clear();
    }

    @Override
    public E get() {
        return this.queue.peek();
    }

    @Override
    public List<E> get(long entryCount) {
        List<E> result = new LinkedList<E>();
        Iterator<E> iterator = this.queue.iterator();
        long entryCurrentCount = 0L;
        while (entryCurrentCount < entryCount && iterator.hasNext()) {
            entryCurrentCount += 1;
            result.add(iterator.next());
        }
        return result;
    }

    @Override
    public List<E> getAll() {
        return new LinkedList<E>(this.queue);
    }

    @Override
    public E pool() {
        return this.queue.poll();
    }

    @Override
    public List<E> pool(long entryCount) {
        LinkedList<E> result = new LinkedList<E>();
        for (int counter = 0; counter < entryCount; counter++) {
            E entry = queue.poll();
            if (entry == null) break;
            result.add(entry);
        }
        return result;
    }

    @Override
    public List<E> poolAll() {
        LinkedList<E> result = new LinkedList<E>();
        while (true) {
            E entry = this.pool();
            if (entry != null) {
                result.add(entry);
            } else {
                break;
            }
        }
        return result;
    }

    @Override
    public void add(E entry) {
        this.queue.add(entry);
    }

    @Override
    public long addMany(List<E> entries) {
        for (E entry : entries) this.add(entry);
        return this.queue.size();
    }
}
