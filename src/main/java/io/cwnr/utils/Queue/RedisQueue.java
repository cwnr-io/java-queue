package io.cwnr.utils.Queue;

import com.fasterxml.jackson.databind.ObjectMapper;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RedisQueue<E> implements Queue<E>{
    private final JedisPool redis;
    private final String queueKey;
    private final ObjectMapper objectMapper;
    private final Class<E> eClass;

    public RedisQueue(JedisPool redis, String queueKey, Class<E> eClass) {
        this.redis = redis;
        this.queueKey = queueKey;
        this.objectMapper = new ObjectMapper();
        this.eClass = eClass;
    }

    @Override
    public long size() throws Exception {
        Long result;
        Jedis jedis = this.redis.getResource();
        if (jedis != null && jedis.ping().equals("PONG")) {
            result = jedis.llen(this.queueKey);
            jedis.close();
        } else throw new Exception("Error getting RedisQueue size");
        return result;
    }

    @Override
    public boolean isEmpty() throws Exception {
        boolean result;
        Jedis jedis = this.redis.getResource();
        if (jedis != null && jedis.ping().equals("PONG")) {
            result = jedis.llen(this.queueKey) == 0L;
            jedis.close();
        } else throw new Exception("Can't get RedisQueue size");
        return result;
    }

    @Override
    public void clear() throws Exception {
        Jedis jedis = this.redis.getResource();
        if (jedis != null && jedis.ping().equals("PONG")) {
            jedis.del(this.queueKey);
            jedis.close();
        } else throw new Exception("Can't clear RedisQueue");
    }

    @Override
    public E get() throws Exception {
        List<E> results = this.get(1);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        } else return null;
    }

    @Override
    public List<E> get(long entryCount) throws Exception {
        List<E> result = new ArrayList<E>();
        Jedis jedis = this.redis.getResource();
        if (jedis != null && jedis.ping().equals("PONG")) {
            List<String> serializedResults = jedis.lrange(this.queueKey, 0L, entryCount - 1);
            jedis.close();
            if (serializedResults != null && !serializedResults.isEmpty()) {
                //We got elements
                for (String serializedResult : serializedResults) {
                    result.add(objectMapper.readValue(serializedResult, this.eClass));
                }
            }
        } else throw new Exception("Can't get elements from RedisQueue");
        return result;
    }

    @Override
    public List<E> getAll() throws Exception {
        return this.get(this.size());
    }

    @Override
    public E pool() throws Exception {
        List<E> results = this.pool(1);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        } else return null;
    }

    @Override
    public List<E> pool(long entryCount) throws Exception {
        List<E> result = new ArrayList<E>();
        Jedis jedis = this.redis.getResource();
        if (jedis != null && jedis.ping().equals("PONG")) {
            List<String> serializedResults = jedis.lpop(this.queueKey, (int) entryCount);
            jedis.close();
            if (serializedResults != null && !serializedResults.isEmpty()) {
                //We got elements
                for (String serializedResult : serializedResults) {
                    result.add(objectMapper.readValue(serializedResult, eClass));
                }
            }
        } else throw new Exception("Can't pool elements from RedisQueue");
        return result;
    }

    @Override
    public List<E> poolAll() throws Exception {
        return this.pool(this.size());
    }

    @Override
    public void add(E entry) throws Exception {
        LinkedList<E> entries = new LinkedList<>();
        entries.add(entry);
        this.addMany(entries);
    }

    @Override
    public long addMany(List<E> entries) throws Exception {
        long result;

        if (entries == null || entries.isEmpty()) {
            this.size();
        }

        Jedis jedis = this.redis.getResource();
        if (jedis != null && jedis.ping().equals("PONG") && entries != null) {
            for (E entry : entries) {
                jedis.rpush(this.queueKey, objectMapper.writeValueAsString(entry));
            }
        } else throw new Exception("Can't add elements to RedisQueue");
        result = this.size();
        jedis.close();
        return result;
    }
}
