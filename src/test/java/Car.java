import java.util.Objects;

public class Car {
    private Model model;
    private long mileage;
    private String registration;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return mileage == car.mileage && Objects.equals(model, car.model) && Objects.equals(registration, car.registration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, mileage, registration);
    }

    @Override
    public String toString() {
        return "Car{" +
                "model=" + model +
                ", mileage=" + mileage +
                ", registration='" + registration + '\'' +
                '}';
    }

    public Car(){}

    public Car(Model model, long mileage, String registration) {
        this.model = model;
        this.mileage = mileage;
        this.registration = registration;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public long getMileage() {
        return mileage;
    }

    public void setMileage(long mileage) {
        this.mileage = mileage;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }
}