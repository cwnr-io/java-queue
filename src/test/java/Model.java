import java.util.Objects;

public class Model {
    public String company;
    public String model;

    public Model(){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Model model1 = (Model) o;
        return company.equals(model1.company) && model.equals(model1.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(company.hashCode(), model.hashCode());
    }

    @Override
    public String toString() {
        return "Model{" +
                "company='" + company + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    public Model(String company, String model) {
        this.company = company;
        this.model = model;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}