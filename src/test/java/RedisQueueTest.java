import io.cwnr.utils.Queue.InternalQueue;
import io.cwnr.utils.Queue.Queue;
import io.cwnr.utils.Queue.RedisQueue;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;

public class RedisQueueTest {
    @Test
    void CreateEmptyQueues() {
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "127.0.0.1", 6379);
        Queue<Long> queueOfLong = new RedisQueue<>(jedisPool, "Q_LONG", Long.class);
        Queue<String> queueOfString = new RedisQueue<>(jedisPool, "Q_STRING", String.class);
        Queue<Car> queueOfCar = new RedisQueue<>(jedisPool, "Q_CAR", Car.class);

        try {
            assert (queueOfLong.isEmpty());
            assert (queueOfLong.size() == 0);
            assert (queueOfString.isEmpty());
            assert (queueOfString.size() == 0);
            assert (queueOfCar.isEmpty());
            assert (queueOfCar.size() == 0);
        } catch (Exception ignored) {}
        jedisPool.close();
    }

    @Test
    void ClearQueues() {
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "127.0.0.1", 6379);
        Queue<Long> queueOfLong = new RedisQueue<>(jedisPool, "Q_LONG", Long.class);
        Queue<String> queueOfString = new RedisQueue<>(jedisPool, "Q_STRING", String.class);
        Queue<Car> queueOfCar = new RedisQueue<>(jedisPool, "Q_CAR", Car.class);

        Model model = new Model("Audi", "A4");
        Car car = new Car(model, 123000, "FAKE");

        try {
            queueOfLong.add(0L);
            queueOfString.add("TEST");
            queueOfCar.add(car);

            assert queueOfLong.size() == 1;
            assert queueOfString.size() == 1;
            assert queueOfCar.size() == 1;

            assert !queueOfLong.isEmpty();
            assert !queueOfString.isEmpty();
            assert !queueOfCar.isEmpty();

            queueOfLong.clear();
            queueOfString.clear();
            queueOfCar.clear();

            assert queueOfLong.size() == 0;
            assert queueOfString.size() == 0;
            assert queueOfCar.size() == 0;

            assert queueOfLong.isEmpty();
            assert queueOfString.isEmpty();
            assert queueOfCar.isEmpty();

        } catch (Exception ignored) {}
        jedisPool.close();
    }

    @Test
    void testQueue() {
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), "127.0.0.1", 6379);
        Queue<Long> queueOfLong = new RedisQueue<>(jedisPool, "Q_LONG", Long.class);
        Queue<String> queueOfString = new RedisQueue<>(jedisPool, "Q_STRING", String.class);
        Queue<Car> queueOfCar = new RedisQueue<>(jedisPool, "Q_CAR", Car.class);

        Model model11 = new Model("Audi", "A1");
        Car car11 = new Car(model11, 123001, "FAKE1");

        Model model12 = new Model("Audi", "A1");
        Car car12 = new Car(model12, 123001, "FAKE1");

        Model model21 = new Model("Audi", "A2");
        Car car21 = new Car(model21, 123001, "FAKE2");

        Model model22 = new Model("Audi", "A2");
        Car car22 = new Car(model22, 123001, "FAKE2");

        try {
            Long singleLong = 1L;
            List<Long> listLong = List.of(1L, 2L, 2L);

            String singleString = "String 1";
            List<String> listString = List.of("String 1","String 2","String 2");

            List<Car> listCar = List.of(car12, car21, car22);

            // Longs
            assert queueOfLong.get() == null;
            queueOfLong.add(singleLong);
            assert queueOfLong.get().equals(singleLong);
            assert queueOfLong.pool().equals(singleLong);
            assert queueOfLong.isEmpty();
            queueOfLong.addMany(listLong);
            assert queueOfLong.size() == 3;
            assert queueOfLong.getAll().equals(listLong);
            assert queueOfLong.get(2).equals(listLong.subList(0,2));
            assert queueOfLong.pool(2).equals(listLong.subList(0,2));
            assert queueOfLong.poolAll().size() == 1;

            // Strings
            assert queueOfString.get() == null;
            queueOfString.add(singleString);
            assert queueOfString.get().equals(singleString);
            assert queueOfString.pool().equals(singleString);
            assert queueOfString.isEmpty();
            queueOfString.addMany(listString);
            assert queueOfString.size() == 3;
            assert queueOfString.getAll().equals(listString);
            assert queueOfString.get(2).equals(listString.subList(0,2));
            assert queueOfString.pool(2).equals(listString.subList(0,2));
            assert queueOfString.poolAll().size() == 1;

            // Strings
            assert queueOfCar.get() == null;
            queueOfCar.add(car11);
            assert queueOfCar.get().equals(car11);
            assert queueOfCar.pool().equals(car11);
            assert queueOfCar.isEmpty();
            queueOfCar.addMany(listCar);
            assert queueOfCar.size() == 3;
            assert queueOfCar.getAll().equals(listCar);
            assert queueOfCar.get(2).equals(listCar.subList(0,2));
            assert queueOfCar.pool(2).equals(listCar.subList(0,2));
            assert queueOfCar.poolAll().size() == 1;
            assert queueOfCar.poolAll().isEmpty();

        } catch (Exception ignored) {}
        jedisPool.close();
    }
}
