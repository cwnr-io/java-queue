import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
public class ModelTest {

    Model model = new Model("Audi", "A4");
    Model model2 = new Model("Audi", "A4");
    Model model3 = new Model("Audi", "A5");

    @Test
    void EqualityTest() {
        Assertions.assertEquals(model, model2);
        Assertions.assertEquals(model2, model);
    }

    @Test
    void NotEqualityTest() {
        Assertions.assertNotEquals(model, model3);
        Assertions.assertNotEquals(model2, model3);
        Assertions.assertNotEquals(model3, model);
        Assertions.assertNotEquals(model3, model2);
    }
}
